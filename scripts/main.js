//****************    VARIABLES    ****************
var pi = Math.PI
var angleMove = 0.06
var moveCondition = false
var possiblyPlayers = [
    {nick: "Player 1", color: 0, key: "ArrowUp" },
    {nick: "Player 2", color: 1, key: "w" },
    {nick: "Player 3", color: 2, key: "c" },
    {nick: "Player 4", color: 3, key: "n" },
]

var possiblyColors = ["blue", "red", "pink", "green"]
var playersPanels = []
var activePlayersValues = [
    {
        nick: "Admin",
        key: " ",
        color: "black"
    }
]
var activePlayers = []
//****************    FUNCTIONS    ****************

/*
input: minimum and maximum value of tab
output: rounded value of random number
*/
function randomize(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min
}

function game(){
    ctx.strokeStyle="#000000";
    animate()
}

function headerGenerate(){
    var header = document.createElement("h1")
    header.innerHTML = "PRESS SPACE TO START THE GAME"
    header.id = "header"
    document.body.append(header)
}

function gamePanelGenerate(){
    //website positioning
    var flex = document.createElement("div")
    flex.id = "flex"
    document.body.append(flex)
    //canvas (gamefield) generator
    canvasGenerate()
    //player panel
    var playerPanel = document.createElement("div")
    playerPanel.id = "playerPanel"
    flex.append(playerPanel)
    //player panel fill
    for(var i=0; i<possiblyPlayers.length; i++){
        playersPanels.push(new _Panel(i, possiblyPlayers[i].key, possiblyColors[i]))
    }
}

function animate() {
    if(moveCondition){
        for(var i=0; i<activePlayers.length; i++)
            activePlayers[i].moveUpdate()
    }
    requestAnimationFrame(animate);
}

function searchIndex(arr, type, value){     //szukanie pierwszego w obiekcie powtórzenia danej wartości
    for(var i=0; i<arr.length; i++){
        if(arr[i][type] == value)
            return i
    }
    return -1
}
/*
action: page onload
*/
document.addEventListener("DOMContentLoaded", function game(event){
    headerGenerate()
    gamePanelGenerate()
})
