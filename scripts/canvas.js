function pathGenerate(){
    
    outer = new Path2D();
    outer.moveTo(200, 398);
    outer.arc(200, 200, 198, 0.5*pi, 1.5*pi)
    outer.lineTo(600,2)
    outer.arc(600, 200, 198, 1.5*pi, 0.5*pi)
    outer.lineTo(200,398)

    ctx.strokeStyle="#FF0000";
    ctx.stroke(outer);

    internal = new Path2D();
    internal.moveTo(230, 300);
    internal.arc(230, 200, 100, 0.5*pi, 1.5*pi)
    internal.lineTo(570,100)
    internal.arc(570, 200, 100, 1.5*pi, 0.5*pi)
    internal.lineTo(230,300)
    
    ctx.strokeStyle="#FF0000";
    ctx.stroke(internal);
    startGenerate()
}

function startGenerate(){
    start = new Path2D()
    //januszowe rozwiązanie
    start.moveTo(300, 300);
    start.lineTo(300,398)
    start.moveTo(301,398)
    start.lineTo(301,300)
    start.moveTo(302, 300);
    start.lineTo(302,398)
    ctx.strokeStyle="#0000FF";
    ctx.stroke(start);
    game()
}

function internalBarrier(){
    ctx.beginPath()
    ctx.moveTo(230,300)
    ctx.arc(230, 200, 100, 0.5*pi, 1.5*pi)
    ctx.arc(570, 200, 100, 1.5*pi, 0.5*pi)
    ctx.closePath();
    var patternMiddle = ctx.createPattern(background, "repeat")
    ctx.fillStyle = patternMiddle
    ctx.fill()
    ctx.stroke()
    pathGenerate()
}

function outerBarrier(){
    //outer barrier 
    ctx.beginPath();
    ctx.arc(200, 200, 198, 0.5*pi, 1.5*pi)
    ctx.arc(600, 200, 198, 1.5*pi, 0.5*pi)
    ctx.closePath()
    var pattern = ctx.createPattern(dirt, "repeat")
    ctx.fillStyle = pattern
    ctx.fill()
    ctx.lineWidth=3
    ctx.stroke()
    //internal barrier
    internalBarrier()
}

function backgroundCanvas(){
        ctx.drawImage(background,0,0, 800, 400);
        //outer barrier
        outerBarrier()
}
/*
action: generating canvas
*/
function canvasGenerate(){
    //canvas element
    var canvasDiv =  document.createElement("div")
    canvasDiv.id = "canvasDiv"
    canvasDiv.style.zIndex = "1"
    document.getElementById("flex").append(canvasDiv)

    let canvas = document.createElement("canvas")
    canvas.id = "main"
    canvas.width = 800
    canvas.height = 400
    canvasDiv.append(canvas)
    ctx = canvas.getContext('2d');
    //background image
    background = new Image();
    background.src = "img/grass.jpg";
    background.onload = function () {
        dirt = new Image()
        dirt.src = "img/dirt.jpg"
        dirt.onload = function(){
            backgroundCanvas()
        }
    }
}