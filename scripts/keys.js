//space listener for start game
document.addEventListener("keydown", function(click){
    if(click.key == " " && activePlayers.length != 0 && !(document.getElementById("shadowOverlay")) && click.target.localName != "input" ){
        if(moveCondition == false){
            moveCondition = true
            document.getElementById("header").innerHTML = " "
            for(var i=0; i<playersPanels.length; i++){
                //tworzenie panelów postępu gry
                playersPanels[i].changeKey.blur()
                playersPanels[i].gameStatus = document.createElement("div")
                playersPanels[i].gameStatus.classList.add("gameStatus")
                if(playersPanels[i].player){
                    playersPanels[i].gameStatus.append(playersPanels[i].lapShow)
                    playersPanels[i].player.lapShow = playersPanels[i].lapShow
                    playersPanels[i].gameStatus.style.top = "-100%"
                }else
                    playersPanels[i].gameStatus.style.top = "-200%" 
                playersPanels[i].upperDiv.append(playersPanels[i].gameStatus)
            }
        }
    }else if(click.target.localName == "input" && click.keyCode == 32)  //dodawanie spacji w inpucie
        click.target.value+= " "
})

//disable key scroll functions
window.addEventListener("keydown", function(e) {
    // space and arrow keys
    if([32, 37, 38, 39, 40].indexOf(e.keyCode) > -1) {
        e.preventDefault();
    }
}, false);