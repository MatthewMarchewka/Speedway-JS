function _Panel(i, key, color) {
    this.player = null
    this.elInArr = null
    //elements
    this.upperDiv = null 
    this.panel = null
    this.nickInput = null
    this.funcKeyOutput = null
    this.changeKey = null
    this.colorSwitch = []
    this.shadow = null
    this.lapShow = null
    //game status
    this.gameStatus = null
    //info
    this.numberPlayer = i
    this.key = key
    this.color = color

    this.create = function () {         //fizyczne tworzenie obiektów
        //lap show header, in use later
        this.lapShow = document.createElement("h1")
        this.lapShow.innerHTML = "- 1/5 -<br> Do końca: " + 5
        //upper div
        this.upperDiv =  document.createElement("div")
        this.upperDiv.classList.add("upperDiv")
        playerPanel.append(this.upperDiv)
        //Generate panel with user info
        this.panel =  document.createElement("div")
        this.panel.classList.add("playerInfo")
        this.upperDiv.append(this.panel)
        //left side of panel
        var nickPanel = document.createElement("div")
        nickPanel.classList.add("nickPanel")
        this.panel.append(nickPanel)
        this.nickInput = document.createElement("input")
            this.nickInput.classList.add("playerNick")
            this.nickInput.value = possiblyPlayers[i].nick
            var funcChangeNick = function(object){
                return function(){
                    if(!(searchIndex(activePlayersValues, "nick", object.nickInput.value) == -1))
                        object.nickInput.value = "Already taken"
                    activePlayersValues[ searchIndex(activePlayersValues, "nick", object.player.nick) ].nick = object.nickInput.value
                    object.player.nick = object.nickInput.value
                }
            }
            this.nickInput.addEventListener("change", funcChangeNick(this))
            nickPanel.appendChild(this.nickInput)
            var playerKey = document.createElement("div")
            playerKey.classList.add("playerKey")
            nickPanel.appendChild(playerKey)
                this.funcKeyOutput = document.createElement("h3")
                this.funcKeyOutput.innerHTML = "Functional key: " + possiblyPlayers[i].key 
                playerKey.appendChild(this.funcKeyOutput)
                this.changeKey = document.createElement("button")
                this.changeKey.innerHTML = "Change key"
                var funcChangeKey = function(object){
                    return function () {
                        if(!(document.getElementById("shadowOverlay"))){
                            //shadow overlay
                            var shadowOverlay =  document.createElement("div")
                            shadowOverlay.id = "shadowOverlay"
                            var removeShadowOverlay = function(){
                                if(document.getElementById("shadowOverlay"))
                                    document.getElementById("shadowOverlay").remove()   
                                object.upperDiv.style.zIndex = "4"
                                document.removeEventListener("keydown", keySet)
                            }
                            shadowOverlay.addEventListener("click", removeShadowOverlay )
                            document.body.append(shadowOverlay)
                            object.upperDiv.style.zIndex = "5"
                            object.changeKey.focus()
                            //change key listener
                            var keySet = function(e){
                                if(searchIndex(activePlayersValues, "key", e.key) == -1){
                                    activePlayersValues[ searchIndex(activePlayersValues, "key", object.key) ].key = e.key
                                    object.funcKeyOutput.innerHTML = "Functional key: " + e.key
                                    object.key = e.key
                                }
                                removeShadowOverlay()
                            }
                            document.addEventListener("keydown", keySet) 
                        }
                    }
                }
                this.changeKey.addEventListener("click", funcChangeKey(this) );
                playerKey.appendChild(this.changeKey)
        //right side of panel
        var colorPanel = document.createElement("div")
        colorPanel.classList.add("colorPanel")
        this.panel.append(colorPanel)
        for(var j=0; j<4 ; j++){
            var colorPick = document.createElement("div")
            colorPick.classList.add("colorPick")
            colorPick.style.backgroundColor = possiblyColors[j]
            colorPanel.append(colorPick)
            if(j != this.numberPlayer){
                var roundShadow = document.createElement("div")
                roundShadow.classList.add("roundShadow")
                colorPick.append(roundShadow)
            }
            this.colorSwitch.push(colorPick)
        }
        this.shadowCreate()
    }

    var bending = function(object, click){      //wyginanie elementu
        if(click.key == object.key)
            object.player.turn = true
    }
    
    var stopBending = function(object, click){  //koniec wyginania elementu
        if(click.key == object.key)
            object.player.turn = false
    }
        
    this.shadowCreate = function () {         //dodanie cienia do elementu
        //shadow div
        this.shadow = document.createElement("div")
        this.shadow.classList.add("panelShadow")
        this.shadow.onclick = this.shadowRemove.bind(null, this)
        this.upperDiv.append(this.shadow)
        //shadow text
        var shadowTxt = document.createElement("h1")
        shadowTxt.innerHTML = "CLICK TO ADD PLAYER"
        this.shadow.append(shadowTxt)
    }

    this.shadowRemove = function(object,click) {    //dodanie nowego zawodnika
        object.shadow.parentNode.removeChild(object.shadow)
        let playerImg = new Image()
        playerImg.src = "img/player/"+object.color+".png"
        playerImg.alt = "x"
        playerImg.onload = function () {
            object.player = new _Player(object.nickInput.value,320, (310+(object.numberPlayer*25)), object.color, playerImg);
            object.elInArr = {
                nick: object.player.nick,
                key: object.key,
                color: object.color
        }
            activePlayersValues.push(object.elInArr)
            activePlayers.push(object.player)
            document.addEventListener("keydown", bending.bind(null, object))
            document.addEventListener("keyup", stopBending.bind(null, object))
        }
    }

    this.create();
}