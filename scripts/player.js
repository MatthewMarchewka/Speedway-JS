function _Player(nick, pozX, pozY,color, img) {
    //init
    this.pozX = pozX
    this.pozY = pozY
    this.color = color
    this.nick = nick
    this.lapShow = null
    //move
    this.moveX = 3
    this.moveY = 0
    this.angle = pi/2
    this.turn = false
    this.inGame = true
    //info
    this.lap = 1
    //path canvas
    this.pathCanvas = document.createElement("canvas")
    this.pathCanvas.width = 800
    this.pathCanvas.height = 400
    this.pathCanvas.style.zIndex = "2"
    document.getElementById("canvasDiv").append(this.pathCanvas)
    this.pathCtx = this.pathCanvas.getContext('2d')
    //image canvas
    this.canvas = document.createElement("canvas")
    this.canvas.width = 800
    this.canvas.height = 400
    this.canvas.style.zIndex = "3"
    document.getElementById("canvasDiv").append(this.canvas)
    this.ctx = this.canvas.getContext('2d')

    //init image
    this.image = img

    var licznik = 0
    
    function drawImage(context, pozX, pozY, angle, image){
        context.save();
        context.translate(pozX,pozY)
        //context.translate(16,35.5)
        context.rotate(-angle);
        context.drawImage(image, -16, -56);
        context.restore();
    }

    function disappearance(pathCtx){
        var myImageData = pathCtx.getImageData(0, 0, 800, 400);
        var data = myImageData.data
        for(var i=3; i<data.length; i=i+4){
            data[i] = data[i] * 0.91
            if(data[i] < 60){
                data[i] = 0
            }
        }
        pathCtx.putImageData(myImageData, 0, 0);
    }

    function drawPath(pathCtx, pozX, pozY, moveX, moveY, color){
        if(licznik == 10){
            licznik = 0
            disappearance(pathCtx)
        }
        pathCtx.save()
        pathCtx.beginPath();
        pathCtx.moveTo(pozX, pozY);
        pathCtx.lineTo(pozX + moveX, pozY + moveY);
        pathCtx.lineWidth = 2;
        pathCtx.strokeStyle= color
        pathCtx.stroke();
        pathCtx.closePath();
        licznik++;
    }

    this.moveUpdate = function(){
        if(ctx.isPointInPath(internal , this.pozX, this.pozY ) || !(ctx.isPointInPath(outer , this.pozX, this.pozY )) ){
            this.inGame = false
            activePlayers.splice( searchIndex(activePlayers, "nick", this.nick) , 1);
            if(activePlayers.length == 0){
                window.alert("Koniec gry! Najdalej dojechał: "+this.nick)
                location.reload(); 
            }
        }else if(ctx.isPointInPath(start, Math.round(this.pozX), this.pozY)){
            if(this.lap == 5){  //ostatnie okrążenie
                window.alert("Koniec gry! Wygrał: "+this.nick)
                location.reload(); 
            }else{
                this.lap++
                this.lapShow.innerHTML = "- "+this.lap+"/5 - <br> Do końca: " + (6-this.lap)
            }
        }
        if(this.inGame){
            //image
            this.ctx.clearRect(0,0,800,400)
            //rysowanie motocykla w nowej pozycji
            drawImage(this.ctx,this.pozX,this.pozY,this.angle,this.image)
            //path
            drawPath(this.pathCtx, this.pozX, this.pozY, this.moveX, this.moveY, this.color)
            this.pozX += this.moveX;
            this.pozY += this.moveY;
            //wyginanie
            if (this.turn) {
                this.angle += 0.04;
                //z własności trójkąta prostokątnego
                this.moveY = 3 * Math.cos(this.angle);
                this.moveX = 3 * Math.sin(this.angle);
            }
        }
    }
    this.moveUpdate()
}